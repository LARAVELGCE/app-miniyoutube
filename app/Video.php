<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected  $table = 'videos';

    // Relacion de Muchos a Uno
    // Muchos videos pertenecen a usuario
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    // uno a muchos

    // un video tiene muchos comentarios
    public function comments()
    {
        return $this->hasMany(Comment::class);

    }

}
