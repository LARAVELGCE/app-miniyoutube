<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        $user = new User();
        $user->role        = "rol1";
        $user->name        = "Geancarlos CE";
        $user->username    = "devgce";
        $user->image       =  "http://lorempixel.com/400/400/";
        $user->email       =  "geancarlosce96@gmail.com";
        $user->password    = bcrypt('secret');
        $user->save();

        $users = factory(\App\User::class,20)->create();
        $users->each(function ($user)
        {
            $videos = factory(\App\Video::class,5)->make();
            $user->videos()->saveMany($videos);

            $videos->each(function ($c)
            {
                $comments = factory(\App\Comment::class,5)->make();
                $c->comments()->saveMany($comments);

            });


        });

    }
}
