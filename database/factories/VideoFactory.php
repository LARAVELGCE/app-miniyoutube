<?php

use Faker\Generator as Faker;

$factory->define(App\Video::class, function (Faker $faker) {
    return [

'title'               =>  substr($faker->sentence(3) , 0,-1) ,
'description'         =>  $faker->sentence(10),
'status'              =>  $faker->randomElement($array = ['activo','inactivo']),
'image'               =>  $faker->imageUrl(400, 400),
'video_path'          =>  'https://www.youtube.com/watch?v=cAWgoJC7TzI',
'user_id'             =>   $faker->numberBetween(1,21),
    ];
});
