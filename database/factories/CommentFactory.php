<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [

'body'       =>   $faker->sentence(10),
'user_id'    =>   $faker->numberBetween(1,21),
'video_id'   =>   $faker->numberBetween(1,100),

    ];
});
